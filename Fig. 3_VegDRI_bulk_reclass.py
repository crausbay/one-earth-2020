#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      ianle
#
# Created:     28/11/2018
# Copyright:   (c) ianle 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    pass

import arcpy
import glob
import os
import pandas as pd
import csv
from arcpy import env
from arcpy.sa import *

arcpy.CheckOutExtension("Spatial")
arcpy.env.overwriteOutput = True


#Input raster format - "D:\\Shelly\\bda\\Bulk963720\\VegDRI\\bpasg_emodis_week17_042609.tif"

# Local variables:
workspace = "D:\\Shelly\\data\\VegDri\\Derived"
arcpy.env.workspace = r'D:\Shelly\data\VegDri\Derived'
Drought_folder = "D:\\Shelly\\data\\VegDri\\Derived\\"
Data_available_folder = "D:\\Shelly\\data\\VegDri\\Derived\\"


str1 = "D:\\Shelly\\bda\\"
str2 = "\\VegDRI"



folder_list = ["Bulk963720", "Bulk964102", "Bulk964103", "Bulk964104",
    "Bulk964105", "Bulk964106", "Bulk964107", "Bulk964108", "Bulk964109",
    "Bulk964111"]

for folder in folder_list:
    path = str1 + folder + str2
    print (path)

    #path = r"D:\\Shelly\\bda\\Bulk963720\\VegDRI"

    raster_list = glob.glob(os.path.join(path, "vegdri_emodis*.tif"))     # advisable to use os.path.join as this makes concatenation OS independent

#example file name for reclass raster \\Drought_week52_122709.tif, \\Data_available_week52_122709.tif

    for raster in raster_list:
        in_raster = os.path.join(raster)
        file_base = os.path.basename(raster)
        out = file_base.replace('vegdri_emodis', '\Drought')
        out_path = workspace + out
        out2 = file_base.replace('vegdri_emodis', '\Data_count')
        out_path2 = workspace + out2

        #Process: Reclassify (1)
        arcpy.gp.Reclassify_sa(in_raster, "Value", "1 64 1;64 80 1;80 96 0;96 112 0;112 160 0;160 167 0;167 176 0;176 192 0;192 253 NODATA;253 254 NODATA;254 255 NODATA", out_path, "DATA")

        # Process: Reclassify (2)
        arcpy.gp.Reclassify_sa(in_raster, "Value", "1 64 1;64 80 1;80 96 1;96 112 1;112 160 1;160 167 1;167 176 1;176 192 1;192 253 NODATA;253 254 NODATA;254 255 NODATA", out_path2, "DATA")

        #print raster
        print (in_raster)
        print(out_path)
        print(out_path2)




#raster_drought = glob.glob(os.path.join(workspace, "drought*"))

rasterList = arcpy.ListRasters("Drought*", "TIF")

# Execute CellStatistics
outCellStatistics = CellStatistics(rasterList, "SUM", "DATA")

# Save the output
outCellStatistics.save("D:\\Shelly\\data\\VegDri\\Derived\out\Drought_add.tif")


rasterList2 = arcpy.ListRasters("Data*", "TIF")

# Execute CellStatistics
outCellStatistics = CellStatistics(rasterList2, "SUM", "DATA")

# Save the output
outCellStatistics.save("D:\\Shelly\\data\\VegDri\\Derived\out\Data_add.tif")

#last divide raster to get percentage python snipet from arcgis... run in arcgis not from code, could add this to code
#arcpy.gp.RasterCalculator_sa('("Drought_add.tif" * 1.0) /("Data_add.tif" * 1.0)', "D:/Shelly/data/VegDri/Derived/out/Drought_pct.tif")





if __name__ == '__main__':
    main()
