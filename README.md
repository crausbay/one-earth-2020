# One Earth 2020

Data and code for figures presented in the peer-reviewed manuscript for One Earth: Unfamiliar territory: emerging themes for ecological drought research and management